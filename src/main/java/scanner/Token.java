/*
 * Copyright (c) 2012, Andreas Donig
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package scanner;

import static scanner.util.Arguments.throwIllegalArgumentExceptionIfStringIsEmpty;
import static scanner.util.Arguments.throwNullPointerExceptionIfObjectIsNull;

public final class Token {

    private final String tokenName;
    private final Object attributeValue;

    public Token(final String tokenName, final Object attributeValue) {
        throwNullPointerExceptionIfObjectIsNull(tokenName, "tokenName");
        throwIllegalArgumentExceptionIfStringIsEmpty(tokenName, "tokenName");
        this.tokenName = tokenName;
        this.attributeValue = attributeValue;
    }

    public Token(final String tokenName) {
        this(tokenName, null);
    }

    public String getTokenName() {
        return tokenName;
    }

    public Object getAttributeValue() {
        return attributeValue;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Token");
        sb.append("{tokenName='").append(tokenName).append('\'');
        sb.append(", attributeValue='").append(attributeValue).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
