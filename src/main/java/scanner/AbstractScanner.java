/*
 * Copyright (c) 2012, Andreas Donig
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package scanner;

import scanner.exception.RollBackException;

import java.io.IOException;
import java.io.InputStreamReader;

import static scanner.util.Arguments.throwIllegalArgumentExceptionIfComparableIsSmallerThanOrEqualTo;
import static scanner.util.Arguments.throwNullPointerExceptionIfObjectIsNull;

public abstract class AbstractScanner implements Scanner {

    protected static final char EOF = '\0';

    protected char[] buffer;

    protected int input = 0;

    private int fence = 0;

    private final InputStreamReader stream;

    protected AbstractScanner(final InputStreamReader stream) {
        throwNullPointerExceptionIfObjectIsNull(stream, "stream");
        this.stream = stream;
    }

    public void initialize(final int bufferSize) throws IOException {
        throwIllegalArgumentExceptionIfComparableIsSmallerThanOrEqualTo(bufferSize, "bufferSize", 1);
        fillBuffer(0, bufferSize);
    }

    protected char getNextChar() throws IOException {
        final char c = buffer[input];
        final int n = buffer.length;
        input = (input + 1) % (2 * n);
        if (input % n == 0) {
            fillBuffer(input, n - 1);
            fence = (input + n) % (2 * n);
        }
        return c;
    }

    protected void rollBack() throws RollBackException {
        if (input == fence) {
            throw new RollBackException();
        }
        assert input > 0;
        input = (input - 1) % (2 * buffer.length);
    }

    private void fillBuffer(final int offset, final int length) throws IOException {
        assert offset >= 0;
        assert length > 0;
        final char[] array = new char[offset + length];
        for (int i = 0; i < length; ++i) {
            final int c = stream.read();
            if (c == -1) {
                array[offset + i] = EOF;
                break;
            }
            array[offset + i] = (char) c;
        }
        if (buffer != null) {
            System.arraycopy(buffer, 0, array, 0, offset);
        }
        buffer = array;
    }
}
