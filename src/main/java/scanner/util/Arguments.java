/*
 * Copyright (c) 2012, Andreas Donig
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package scanner.util;

public abstract class Arguments {

    private Arguments() {
        throw new AssertionError("Utility class Arguments should not be instantiated.");
    }

    public static void throwNullPointerExceptionIfObjectIsNull(final Object argument,
                                                               final String argumentName) {
        if (argument == null) {
            final StringBuilder messageBuilder = new StringBuilder();
            messageBuilder.append("Argument ");
            messageBuilder.append('\'').append(argumentName).append('\'');
            messageBuilder.append(" must not be null.");
            throw new NullPointerException(messageBuilder.toString());
        }
    }

    public static void throwIllegalArgumentExceptionIfStringIsEmpty(final String argument,
                                                                    final String argumentName) {
        throwNullPointerExceptionIfObjectIsNull(argument, "argument");
        if (argument.isEmpty()) {
            final StringBuilder messageBuilder = new StringBuilder();
            messageBuilder.append("Argument ");
            messageBuilder.append('\'').append(argumentName).append('\'');
            messageBuilder.append(" must not be empty.");
            throw new NullPointerException(messageBuilder.toString());
        }
    }

    public static <T> void throwIllegalArgumentExceptionIfComparableIsSmallerThanOrEqualTo(final Comparable<T> argument,
                                                                                           final String argumentName,
                                                                                           final T t) {
        throwNullPointerExceptionIfObjectIsNull(argument, "argument");
        if (argument.compareTo(t) < 0) {
            final StringBuilder messageBuilder = new StringBuilder();
            messageBuilder.append("Argument ");
            messageBuilder.append('\'').append(argumentName).append('\'');
            messageBuilder.append(" must be greater than ");
            messageBuilder.append('\'').append(t).append('\'');
            messageBuilder.append('.');
            throw new NullPointerException(messageBuilder.toString());
        }
    }
}
