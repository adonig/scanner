/*
 * Copyright (c) 2012, Andreas Donig
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package scanner.tabledriven;

import scanner.AbstractScanner;
import scanner.Scanner;
import scanner.StateDescriptor;
import scanner.Token;
import scanner.exception.LexicalException;
import scanner.exception.ScannerException;
import scanner.tabledriven.tables.TokenTypeTable;
import scanner.tabledriven.tables.TransitionTable;
import scanner.util.Pair;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public final class MaximalMunchScanner extends AbstractScanner implements Scanner {

    private final Queue<Pair<StateDescriptor, Integer>> stack = new LinkedList<Pair<StateDescriptor, Integer>>();
    private final Set<Pair<StateDescriptor, Integer>> failed = new HashSet<Pair<StateDescriptor, Integer>>();

    private final TransitionTable transitionTable;
    private final TokenTypeTable tokenTypeTable;

    public MaximalMunchScanner(InputStreamReader stream,
                               TransitionTable transitionTable,
                               TokenTypeTable tokenTypeTable) {
        super(stream);
        this.transitionTable = transitionTable;
        this.tokenTypeTable = tokenTypeTable;
    }

    private static boolean isWhitespace(char c) {
        return (c == ' ') || (c == '\n') || (c == '\t');
    }

    @Override
    public Token getNextToken() throws IOException, ScannerException {
        StateDescriptor state = StateDescriptor.getStartState();
        int lexemeBegin = input;
        int lexemeLength = 0;
        stack.clear();
        while (state != StateDescriptor.getErrorState()) {
            final char nextChar = getNextChar();
            if (nextChar == EOF) {
                break;
            }
            if (isWhitespace(nextChar)) {
                if (lexemeLength == 0) {
                    return getNextToken();
                } else {
                    break;
                }
            }
            Pair<StateDescriptor, Integer> p = new Pair<StateDescriptor, Integer>(state, input);
            if (failed.contains(p)) {
                break;
            }
            if (tokenTypeTable.contains(state)) {
                stack.clear();
            }
            stack.add(p);
            state = transitionTable.get(state, nextChar);
        }
        while (!tokenTypeTable.contains(state) && state != null) {
            failed.add(new Pair<StateDescriptor, Integer>(state, input));
            Pair<StateDescriptor, Integer> p = stack.poll();
            assert p != null;
            state = p.getFirst();
            inputPos = p.getSecond();
            truncateLexeme();
            rollBack();
        }
        if (tokenTypeTable.contains(state)) {
            return new Token(tokenTypeTable.get(state), String.valueOf(buffer, lexemeBegin, lexemeLength));
        } else {
            throw new LexicalException();
        }
    }

    private void truncateLexeme() {

    }
}
