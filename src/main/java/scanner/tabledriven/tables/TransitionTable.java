/*
 * Copyright (c) 2012, Andreas Donig
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package scanner.tabledriven.tables;

import scanner.StateDescriptor;

import java.util.HashMap;
import java.util.Map;

import static scanner.util.Arguments.throwNullPointerExceptionIfObjectIsNull;

public final class TransitionTable {
    private final Map<StateDescriptor, Map<Character, StateDescriptor>> transitions = new HashMap<StateDescriptor, Map<Character, StateDescriptor>>();

    public StateDescriptor get(final StateDescriptor d, final char c) {
        throwNullPointerExceptionIfObjectIsNull(d, "d");
        if (transitions.containsKey(d) && transitions.get(d).containsKey(c)) {
            return transitions.get(d).get(c);
        } else {
            return StateDescriptor.getErrorState();
        }
    }

    public void put(final StateDescriptor d, final char c, final StateDescriptor t) {
        throwNullPointerExceptionIfObjectIsNull(d, "d");
        throwNullPointerExceptionIfObjectIsNull(t, "t");
        if (!transitions.containsKey(d)) {
            transitions.put(d, new HashMap<Character, StateDescriptor>());
        }
        transitions.get(d).put(c, t);
    }
}
