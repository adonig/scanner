# Programming language BALAN

## Context Free Syntax

    program      ::= "program" dclrow "begin" stmrow "end"
    dclrow       ::= dcl | dclrow ";" dcl
    dcl          ::= type ident {"," ident}∗
    type         ::= "bool" | "int"
    stmrow       ::= stm {";" stm}∗
    stm          ::= in | out | assign | cond | loop
    in           ::= "input" ident
    out          ::= "output" expr
    assign       ::= ident ":=" expr
    expr         ::= fac | expr "+" fac | expr "–" fac
    fac          ::= prim | prim "*" fac | prim "/" fac
    prim         ::= op | "–" op
    op           ::= bconst | numb | ident | "(" comp ")" | "(" expr ")"
    bconst       ::= "false" | "true"
    comp         ::= expr "=" expr | expr ">" expr
    cond         ::= if_then stmrow "fi" | if_then_else stmrow "fi"
    if_then      ::= "if" expr "then"
    if_then_else ::= if_then stmrow "else"
    loop         ::= "while" expr "do" stmrow "od"

## Lexical Syntax

    ident  ::= letter {letter | digit}*
    letter ::= "A" | ... | "Z" | "a" | ... |"z"
    digit  ::= "0" | ... | "9"
    number ::= {digit}+